#!/bin/bash

OS_R=`uname -r`
PATH7="/srv/www/man"
OUT_PATH6="./index.html"

echo "OS detected: $OS_R"

if [ "$OS_R" == "5.4.13-3-MANJARO" ]; then
   echo 'Settings1'
   PATH7="./scrpt7"
   echo "new path7: $PATH7"
   SCRIPT='gunzip -q --to-stdout {} |  groff -man -T html  2>/dev/null'
else
   echo "opt generic/stock"
   SCRIPT='gunzip -q --to-stdout {} |  groff -man -T html  2>/dev/null'
fi


# Write a command to solve each problem.
# Submit this homework as assignment2.sh to Canvas.

# 1. Use find(1) to find all the files in /usr/share/man
# that start with 'stat'
echo "1."
find /usr/share/man -type f -name "stat*"

# 2. Use find(1) to find all the files in /usr/share/man
# that start with 'stat', sort(1) them, and produce a uniq(1)
# count
echo "2."
find /usr/share/man -type f -name "stat*" | sort | uniq  -c

# 3. Use find(1) to find all the files in /usr/share/man
# that start with 'stat', cut(1) the path and retrieve the
# file's parent directory (IE: man1). sort(1) and perform a 
# a uniq(1) count of those directories.
# Your output should look like the following
# 1 man1
# 6 man2
# 1 man3
echo "3."
find /usr/share/man -type f -name "stat*" | cut -d / -f 5-5  - | sort | uniq -c

# 4. Use find(1) to find all the files in /usr/share/man
# that start with 'stat' then pipe the results to xargs 
# and get the file type, use the file command with the -b
# argument
echo "4."
find /usr/share/man -type f -name "stat*" | xargs file -b

# 5. Use find(1) to find all the files in /usr/share/man
# that start with 'stat' then pipe the results to xargs 
# and compile the files to html
echo "5."
find /usr/share/man -type f -name "stat*" | xargs gunzip -q --to-stdout |  groff -man -T html  2>/dev/null

# 6. Use find(1) to find all the files in /usr/share/man
# that start with 'stat' then pipe the results to xargs 
# and compile the files to html with two processes.
echo "6."
#https://unix.stackexchange.com/a/209250
find /usr/share/man -type f -name "stat*" | xargs  -P 2 -I {} sh -c "$SCRIPT >> $OUT_PATH6"
echo "Output at $OUT_PATH6"  
#rm grohtml-*.png

# Alternate way 
#find /usr/share/man -type f -name "stat*" | xargs -n1 -P2 

# 7. Use find(1) to find all the files in /usr/share/man
# that start with 'stat' then pipe the results to parallel(1) 
# and compile the files to html and
# save the results is /srv/www/man with a .html extension
# Hint: use parallel {}, zcat(1), groff(1) and stdout to an 
# expression that uses basename(1)
echo "7."

mkdir -p $PATH7
#https://unix.stackexchange.com/a/385908
find /usr/share/man -type f -name "stat*" | parallel "$SCRIPT > $PATH7/\`basename {}\`.html"
echo "Done"
