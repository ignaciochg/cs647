EXP="Number  Start   End     Size    File system  Name   Flags"

vgremove -f vg0
mdadm --stop /dev/md0
rm disk*.img

truncate -s 5G disk0.img
truncate -s 5G disk1.img
truncate -s 5G disk2.img

dev0="$(losetup --find --show disk0.img)"
dev1="$(losetup --find --show disk1.img)"
dev2="$(losetup --find --show disk2.img)"

parted -s $dev0 "mklabel gpt"
parted -s $dev1 "mklabel gpt"
parted -s $dev2 "mklabel gpt"


parted -s $dev0  "print free"
parted -s $dev1  "print free"
parted -s $dev2  "print free"


parted -s $dev0 'mkpart pv0p1 ext4 1M 90%'
parted -s $dev1 'mkpart pv1p1 ext4 1M 90%'
parted -s $dev2 'mkpart pv2p1 ext4 1M 90%'

mdadm -C /dev/md0 -l raid1 --metadata=0.90 -n 2 ${dev0}p1 ${dev1}p1 -x 1 ${dev2}p1

pvcreate /dev/md0
vgcreate vg0 /dev/md0
lvcreate -L 1G -n lv0 vg0
lvcreate -L 1G -n lv1 vg0




# 4832.1

# https://www.tecmint.com/parted-command-to-create-resize-rescue-linux-disk-partitions/

