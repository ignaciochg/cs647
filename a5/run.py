#!/usr/bin/env python3 

import sys
import os
import subprocess
import logging
import time

CLEAN = True
myLoggerName = "mylogger"


def run(command, printERR=True):
   process = subprocess.Popen(command.split(), stdout=subprocess.PIPE,stderr=subprocess.PIPE)
   output, error = process.communicate()
   # if printERR and error:
   #    print("ERROR",error)
   return output, error


def cleanup():
   script = """vgremove -f vg0
mdadm --stop /dev/md0
rm disk0.img
rm disk1.img
rm disk2.img"""

   logger.debug("Cleaning up: /dev/md0")
   logger.debug("Cleaning up: disk0.img")
   logger.debug("Cleaning up: disk1.img")
   logger.debug("Cleaning up: disk2.img")


   for line in script.split("\n"):
      # print(line)
      run(line, False)


def createFile(name):
   run("truncate -s 5G "+name)

def losetup(file):
   output, error = run("losetup --find --show "+file)

   if error:
      logger.critical("losetup({}) output error: {}".format(file,error.decode()))
      sys.exit(1)
   else:
      ret = output.decode().replace("\n",'')
      logger.info("losetup({}) output: {}".format(file,ret))
      return ret



def makeGPT(dev):
   command = "parted -s {} 'mklabel gpt'".format(dev)
   logger.info("Creating GPT on {} with: {}".format(dev,command))
   output, error = run(command)
   logger.info("create GPT on {} Command Output: {}".format(dev,output.decode()))
   if error:
      logger.critical("create GPT on {} Command Output: {}".format(dev, error.decode()))
      sys.exit(1)


def makePart(dev,label):
   command = "parted -s {} 'mkpart {} ext4 1M 90%'".format(dev,label)
   logger.info("Creating {} on {} with: {}".format(label,dev,command))
   output, error = run(command)
   logger.info("create {} on {} Command Output: {}".format(label,dev,output.decode()))
   if error:
      logger.critical("create {} on {} Command Output: {}".format(label,dev, error.decode()))
      sys.exit(1)

def makeRAID(devmd, dev0,dev1,EXTdev2):
   command = "mdadm -C {} -l raid1 --metadata=0.90 -n 2 {} {} -x 1 {}".format(devmd,dev0,dev1,EXTdev2)
   logger.info("Creating RAID with: {}".format(command))
   output, error = run(command)
   logger.info("RAID Command Output: {}".format(output.decode()))
   if error and "started." not in error.decode():
      logger.critical("RAID Command Output: {}".format(error.decode()))
      sys.exit(1)



def createPV(dev):
   command = "pvcreate "+dev
   logger.info("Creating pv with: {}".format(command))
   output, error = run(command)
   logger.info("pvcreate Command Output: {}".format(output.decode().replace("\n",'')))
   if error:
      logger.critical("pvcreate Command: {}".format(error.decode()))
      sys.exit(1)

def createVG(name, *devs):
   command = "vgcreate "+name

   for dev in devs:
      command = command + " " + dev

   logger.info("Creating vg with: {}".format(command))
   output, error = run(command)
   logger.info("vgcreate Command Output: {}".format(output.decode().replace("\n",'')))
   if error:
      logger.critical("vgcreate Command: {}".format(error.decode()))
      sys.exit(1)

def createLV(name, size, group):
   command = "lvcreate -L {} -n {} {}".format(size, name, group)
   logger.info("Creating lv with: {}".format(command))
   output, error = run(command)
   logger.info("lvcreate Command Output:{}".format(output.decode().replace("\n",'')))
   if error:
      logger.critical("lvcreate Command: {}".format(error.decode()))
      sys.exit(1)

if __name__ == "__main__":

   if not os.geteuid() == 0:
      sys.exit("Must run as root.")

   logger = logging.getLogger(myLoggerName)
   logger.setLevel(logging.DEBUG)

   formatter = logging.Formatter('%(asctime)s, %(levelname)s, %(filename)s, %(funcName)s, %(message)s')

   out_handler = logging.StreamHandler(sys.stdout)
   out_handler.setFormatter(formatter)
   out_handler.setLevel(logging.DEBUG)

   logger.addHandler(out_handler)

   if CLEAN:
      cleanup()
      run("sync")

   file0 = "disk0.img"
   file1 = "disk1.img"
   file2 = "disk2.img"
   
   logger.info("Filenames: {}, {}, {}".format(file0,file1,file2))

   createFile(file0)
   createFile(file1)
   createFile(file2)



   dev0 = losetup(file0)
   dev1 = losetup(file1)
   dev2 = losetup(file2)


   makeGPT(dev0)
   makeGPT(dev1)
   makeGPT(dev2)


   makePart(dev0,"raidnode0")
   makePart(dev1,"raidnode1")
   makePart(dev2,"raidnode2")
   

   dev0p1 = dev0+"p1"
   dev1p1 = dev1+"p1"
   dev2p1 = dev2+"p1"

   devmd = "/dev/md0"

   makeRAID(devmd, dev0p1, dev1p1, dev2p1)


   createPV(devmd)

   vg = "vg0"

   createVG(vg, devmd)

   createLV("lv0", "1G", vg)
   createLV("lv1", "1G", vg)

