#!/usr/bin/env bash


FS_FILE=/var/local/cs447_home.img
MOUNT_POINT=/home/cs447

#printenv


function createFile {
   echo hi
   truncate -s 2G $1
}


function mountLoopDev {
   #echo "Will mount $1 as loopdev"
#   dev=
   losetup --find --show $1
#   return dev
}


function makeFS {
   parted -s $1 'mklabel gpt'
   parted -s $1 'mkpart cs447-home ext4 1M 100%'
   mkfs.ext4 ${1}p1
}



function mountcs447 {
   echo "Mounting home to /home/cs447"
   mount ${dev}p1 $MOUNT_POINT
}

function cs447own {
   echo "Changing home ownership to cs447"
   chown -R cs447:cs447 /home/cs447
}

function unmountAll {
   echo "Syncing home..."
   sync
   echo "Unmounting home"
   umount -f $MOUNT_POINT
   dev=`losetup | grep "$FS_FILE" | awk '{print $1}'`
   echo "Removing loopdev: $dev"
   if [ -z "$dev" ]; then
      echo "Could not find loop device, could not unmount it"
   else
      losetup -d $dev
   fi

}

function checkCS447_open {
#   printenv
   if [ "$PAM_USER" = "cs447" ]; then
      echo '#####################################'
      echo "Hi $PAM_USER, we are seting up your home."
      #echo "Hello $PAM_USER"
      echo '#####################################'
      cs447UID=`id -u $PAM_USER`
      echo "UID: $cs447UID"
      echo '#####################################'

   else
      echo "you are not CS447"
      exit 0
   fi
}

function checkCS447_close {
#   printenv
   if [ "$PAM_USER" = "cs447" ]; then
      :
      #echo '#####################################'
      #echo 'Will unmount'
      #echo "Hi $PAM_USER, we are seting up your home."
      #echo "Hello $PAM_USER"
      #echo '#####################################'
      #cs447UID=`id -u $PAM_USER`
      #echo "UID: $cs447UID"
      #echo '#####################################'

   else
      #echo "you are not CS447"
      exit 0
   fi
}

function setup_home_cs447 {
   if [ -f "$FS_FILE" ]; then
      echo "$FS_FILE exist"
      dev=`losetup | grep "$FS_FILE" | awk '{print $1}'`
      if [ -z "$dev" ]; then # if file not mounted as loop dev
         dev=`mountLoopDev $FS_FILE`
         echo "Mounted loopdev at $dev"
      else
         echo "Found file at $dev"
      fi
      # FS should already be there
   else # create file, mount oopdev, make fs, change own
      echo "Creating FS for cs447"
      createFile $FS_FILE
      dev=`mountLoopDev $FS_FILE`
      echo $dev
      makeFS ${dev}
      echo '#####################################'
      cs447own
   fi
}




#import os, sys

#for k,v in os.environ.items():
#	print("{}={}".format(k,v))
#sys.exit(0)
