#!/usr/bin/python3

import tarfile # Used to manipulate tarfiles
import sys # Used for writing to stdout
import io # Used for in memory files like StringIO or ByteIO
import tempfile # Used to create temporary files and directories
import os # Used to list files in a directory
import subprocess # Used to run other applications with Popen
import logging # Used for logging

from systemd.journal import JournaldLogHandler

TEMP_PATH = "/tmp/tar_soc/temp/"
DEBUG_MODE = False


##### BEGIN LOGGING SETUP #####

logger = logging.getLogger("tarecho")

formatter = logging.Formatter(
'[%(levelname)s] %(message)s'
)


journald_handler = JournaldLogHandler()
journald_handler.setFormatter(formatter)
journald_handler.setLevel(logging.INFO)


stdoutHandler = logging.StreamHandler(sys.stdout)
stdoutHandler.setLevel(logging.DEBUG)
stdoutHandler.setFormatter(formatter)

logger.addHandler(journald_handler)
if DEBUG_MODE:
   logger.addHandler(stdoutHandler)

logger.setLevel(logging.DEBUG)

##### END OF LOGGING SETUP #####

#Read the binary data from stdin buffer.read()
raw_f = sys.stdin.buffer.read()

#Store it in a ByteIO 'file'
raw_io_f = io.BytesIO(raw_f)

#Open the tar
skip = False
try:
   tar = tarfile.open(fileobj=raw_io_f, mode='r')
except:

   logger.error("Skipping; Recieved input that is not tar.")
   skip = True

if not skip:
   #Create a temporary directory


   if os.path.exists(TEMP_PATH) and os.path.isdir(TEMP_PATH):
      logger.info("Skipping creating of temp directory {}. Already exists.".format(TEMP_PATH))
   elif not os.path.exists(TEMP_PATH):
      os.makedirs(TEMP_PATH)
      logger.info("Created temp directory {}.".format(TEMP_PATH))
   else:
      logger.critical("Creation of the directory {} failed".format(TEMP_PATH))
      sys.exit(1)

   #iterate over the TarInfo objects in the tar and print their
   #names to the journald log
   filenames = list()
   for member in tar.getmembers():
      filenames.append(member.name)
      logger.info("Found File: {}".format(member.name))
      f = tar.extract(member,path=TEMP_PATH)


   #Create a list of absolute paths by joining the temp directory path and the filename
   
   full_files = [TEMP_PATH+item for item in filenames]
   compiled_html = list()

   for full_file in full_files:
      command1 = "/usr/bin/file -b "+full_file
      process1 = subprocess.Popen(command1.split(), stdout=subprocess.PIPE)
      output1, error1 = process1.communicate()

      istroff = output1 == b'troff or preprocessor input, ASCII text\n'
      logger.debug("File {}, is troff: {}".format(full_file, istroff))

      if istroff:
         try:
            command2 = "/usr/bin/groff -man -T html "+full_file
            process2 = subprocess.Popen(command2.split(), stdout=subprocess.PIPE)
            html, error2 = process2.communicate()
            compiled_html.append(html.decode())
            if error2:
               logger.warning("STDERR '{}': \n{}".format(command2,error2))
         except:
            logger.error("Error ocurred: 0x001")

   def smash(lst):
      comp = ""
      for item in lst:
         comp = comp + item + '\n'
      return comp


   #Compile only the troff files and output the html to stdout
   sys.stdout.write(smash(compiled_html))

   # Doing cleanup 
   for file in full_files:
      logger.info("Cleaning up: removing {}".format(file))
      try:
         os.remove(file)
      except:
         logger.error("Failed to remove {}".format(file), exc_info=True)
