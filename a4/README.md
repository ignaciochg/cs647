# Assignment #4

For this assignment, you will be creating a systemd socket unit that iterates over the files in a tar, finds troff files and compiles them to html and outputs them to stdout.   

You will need to turn in three files:   
   
tarecho.socket      - Creates the socket file in /var/run/tarecho   
   
tarecho@.service - Responsible for running /usr/local/bin/compile_troff.py   
   
compile_troff.py  - Iterates over all the files in a tar and compiles and outputs troff files to stdout.   
   
You can download the tar of files with wget.   

```bash 
wget http://cs447.cse.unr.edu/tarecho/troff_files.tar
```
Here is a good start for your compile_troff.py script   

```python
#!/usr/bin/python3

import tarfile # Used to manipulate tarfiles
import sys # Used for writing to stdout
import io # Used for in memory files like StringIO or ByteIO
import tempfile # Used to create temporary files and directories
import os # Used to list files in a directory
import subprocess # Used to run other applications with Popen
import logging # Used for logging

from systemd.journal import JournaldLogHandler

##### BEGIN LOGGING SETUP #####

# get an instance of the logger object this module will use
logger = logging.getLogger("tarecho")

# instantiate the JournaldLogHandler to hook into systemd
journald_handler = JournaldLogHandler()

# set a formatter to include the level name
journald_handler.setFormatter(logging.Formatter(
'[%(levelname)s] %(message)s'
))

# add the journald handler to the current logger
logger.addHandler(journald_handler)

# optionally set the logging level
logger.setLevel(logging.DEBUG)

##### END OF LOGGING SETUP #####

#Read the binary data from stdin buffer.read()
data = sys.stdin.buffer.read()

#Store it in a ByteIO 'file'

#Open the tar

#Create a temporary directory

#iterate over the TarInfo objects in the tar and print their
#names to the journald log

#Extract the archive into the temporary directory

#Get a list of the files

#Create a list of absolute paths by joining the temp directory path and the filename

#Iterate over all the paths and test file type with 'file -b' command

#Compile only the troff files and output the html to stdout
```
