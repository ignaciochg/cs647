#!/usr/bin/env bash


sudo rm -f  /etc/systemd/system/tarecho.socket
sudo rm -f /etc/systemd/system/tarecho@.service
sudo rm -f /usr/local/bin/compile_troff.py


sudo cp {tarecho@.service,tarecho.socket} /etc/systemd/system/
sudo cp compile_troff.py /usr/local/bin/

sudo systemctl daemon-reload
sudo systemctl start tarecho.socket
